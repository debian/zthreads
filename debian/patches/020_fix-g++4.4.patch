Description: fix ftbfs g++4.4
Author: Cleto Martin Angelina <cleto.martin@gmail.com>
Last-Update: 2011-02-16
Index: zthreads/include/zthread/Guard.h
===================================================================
--- zthreads.orig/include/zthread/Guard.h
+++ zthreads/include/zthread/Guard.h
@@ -26,15 +26,15 @@
 #include "zthread/NonCopyable.h"
 #include "zthread/Exceptions.h"
 
-namespace ZThread { 
+namespace ZThread {
 
-// 
-// GuardLockingPolicyContract { 
 //
-// createScope(lock_type&)  
-// bool createScope(lock_type&, unsigned long)  
-// destroyScope(lock_type&)  
-// 
+// GuardLockingPolicyContract {
+//
+// createScope(lock_type&)
+// bool createScope(lock_type&, unsigned long)
+// destroyScope(lock_type&)
+//
 // }
 //
 
@@ -59,12 +59,12 @@ class LockHolder {
 
   template <class T>
   LockHolder(T& t) : _lock(extract(t)._lock), _enabled(true) { }
-  
+
   LockHolder(LockHolder& holder) : _lock(holder._lock), _enabled(true) { }
 
   LockHolder(LockType& lock) : _lock(lock), _enabled(true) { }
 
-  void disable() { 
+  void disable() {
     _enabled = false;
   }
 
@@ -78,7 +78,7 @@ class LockHolder {
 
  protected:
 
-  template <class T>  
+  template <class T>
   static LockHolder& extract(T& t) {
     // Design and Evolution of C++, page 328
     return (LockHolder&)(t);
@@ -117,7 +117,7 @@ class CompoundScope {
         return false;
 
       }
-       
+
     return true;
 
   }
@@ -154,7 +154,7 @@ class LockedScope {
    * @param lock2 LockType1& is the LockHolder that wants to share
   template <class LockType1, class LockType2>
   static void shareScope(LockHolder<LockType1>& l1, LockHolder<LockType2>& l2) {
-    
+
     l2.getLock().acquire();
 
   }
@@ -207,7 +207,7 @@ class LockedScope {
  *
  * Locking policy for Lockable objects. This policy release()s a Lockable
  * when the protection scope is created, and it acquire()s a Lockable
- * when the scope is destroyed. 
+ * when the scope is destroyed.
  */
 class UnlockedScope {
  public:
@@ -251,7 +251,7 @@ class UnlockedScope {
   }
 
 };
- 
+
 
 
 /**
@@ -277,7 +277,7 @@ class TimedLockedScope {
 
     if(!l2.getLock().tryAcquire(TimeOut))
       throw Timeout_Exception();
-       
+
   }
 
   template <class LockType>
@@ -338,29 +338,29 @@ class OverlappedScope {
  * @version 2.2.0
  *
  * Scoped locking utility. This template class can be given a Lockable
- * synchronization object and can 'Guard' or serialize access to 
+ * synchronization object and can 'Guard' or serialize access to
  * that method.
- *  
- * For instance, consider a case in which a class or program have a  
- * Mutex object associated with it. Access can be serialized with a 
+ *
+ * For instance, consider a case in which a class or program have a
+ * Mutex object associated with it. Access can be serialized with a
  * Guard as shown below.
  *
  * @code
  *
  * Mutex _mtx;
  * void guarded() {
- * 
+ *
  *    Guard<Mutex> g(_mtx);
  *
  * }
  *
  * @endcode
  *
- * The Guard will lock the synchronization object when it is created and 
- * automatically unlock it when it goes out of scope. This eliminates 
+ * The Guard will lock the synchronization object when it is created and
+ * automatically unlock it when it goes out of scope. This eliminates
  * common mistakes like forgetting to unlock your mutex.
  *
- * An alternative to the above example would be 
+ * An alternative to the above example would be
  *
  * @code
  *
@@ -385,15 +385,15 @@ class Guard : private LockHolder<LockTyp
   friend class LockHolder<LockType>;
 
 public:
-  
+
   /**
    * Create a Guard that enforces a the effective protection scope
-   * throughout the lifetime of the Guard object or until the protection 
+   * throughout the lifetime of the Guard object or until the protection
    * scope is modified by another Guard.
    *
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
-   * @post the protection scope may be ended prematurely 
+   * @post the protection scope may be ended prematurely
    */
   Guard(LockType& lock) : LockHolder<LockType>(lock) {
 
@@ -403,12 +403,12 @@ public:
 
   /**
    * Create a Guard that enforces a the effective protection scope
-   * throughout the lifetime of the Guard object or until the protection 
+   * throughout the lifetime of the Guard object or until the protection
    * scope is modified by another Guard.
    *
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
-   * @post the protection scope may be ended prematurely 
+   * @post the protection scope may be ended prematurely
    */
   Guard(LockType& lock, unsigned long timeout) : LockHolder<LockType>(lock) {
 
@@ -419,31 +419,31 @@ public:
 
   /**
    * Create a Guard that shares the effective protection scope
-   * from the given Guard to this Guard. 
+   * from the given Guard to this Guard.
    *
-   * @param g Guard<U, V> guard that is currently enabled 
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param g Guard<U, V> guard that is currently enabled
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
    */
   template <class U, class V>
   Guard(Guard<U, V>& g) : LockHolder<LockType>(g) {
 
     LockingPolicy::shareScope(*this, extract(g));
-    
+
   }
 
   /**
    * Create a Guard that shares the effective protection scope
-   * from the given Guard to this Guard. 
+   * from the given Guard to this Guard.
    *
-   * @param g Guard guard that is currently enabled 
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param g Guard guard that is currently enabled
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
    */
   Guard(Guard& g) : LockHolder<LockType>(g) {
 
     LockingPolicy::shareScope(*this, g);
-    
+
   }
 
 
@@ -451,8 +451,8 @@ public:
    * Create a Guard that transfers the effective protection scope
    * from the given Guard to this Guard.
    *
-   * @param g Guard<U, V> guard that is currently enabled 
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param g Guard<U, V> guard that is currently enabled
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
    */
   template <class U, class V>
@@ -467,8 +467,8 @@ public:
    * Create a Guard that transfers the effective protection scope
    * from the given Guard to this Guard.
    *
-   * @param g Guard guard that is currently enabled 
-   * @param lock LockType the lock this Guard will use to enforce its 
+   * @param g Guard guard that is currently enabled
+   * @param lock LockType the lock this Guard will use to enforce its
    * protection scope.
    */
   Guard(Guard& g, LockType& lock) : LockHolder<LockType>(lock) {
@@ -476,7 +476,7 @@ public:
     LockingPolicy::transferScope(*this, g);
 
   }
-  
+
 
   /**
    * Unlock a given Lockable object with the destruction of this Guard
@@ -488,14 +488,14 @@ public:
 
 template <class LockType, class LockingPolicy>
 Guard<LockType, LockingPolicy>::~Guard() throw() {
-    
+
   try {
-    
-    if(!isDisabled())
+
+    if(!LockHolder<LockType>::isDisabled())
       LockingPolicy::destroyScope(*this);
-    
-  } catch (...) { /* ignore */ }  
-  
+
+  } catch (...) { /* ignore */ }
+
 }
 
 
Index: zthreads/src/MutexImpl.h
===================================================================
--- zthreads.orig/src/MutexImpl.h
+++ zthreads/src/MutexImpl.h
@@ -58,14 +58,14 @@ protected:
  * @version 2.2.11
  * @class MutexImpl
  *
- * The MutexImpl template allows how waiter lists are sorted, and 
+ * The MutexImpl template allows how waiter lists are sorted, and
  * what actions are taken when a thread interacts with the mutex
  * to be parametized.
  */
-template <typename List, typename Behavior> 
+template <typename List, typename Behavior>
 class MutexImpl : Behavior {
 
-  //! List of Events that are waiting for notification 
+  //! List of Events that are waiting for notification
   List _waiters;
 
   //! Serialize access to this Mutex
@@ -75,7 +75,7 @@ class MutexImpl : Behavior {
   volatile ThreadImpl* _owner;
 
  public:
-  
+
 
   /**
    * Create a new MutexImpl
@@ -84,11 +84,11 @@ class MutexImpl : Behavior {
    * properly allocated
    */
   MutexImpl() : _owner(0) { }
-  
+
   ~MutexImpl();
 
   void acquire();
-  
+
   void release();
 
   bool tryAcquire(unsigned long timeout);
@@ -98,20 +98,20 @@ class MutexImpl : Behavior {
   /**
    * Destroy this MutexImpl and release its resources
    */
-template<typename List, typename Behavior> 
+template<typename List, typename Behavior>
 MutexImpl<List, Behavior>::~MutexImpl() {
 
 #ifndef NDEBUG
 
     // It is an error to destroy a mutex that has not been released
-    if(_owner != 0) { 
+    if(_owner != 0) {
 
       ZTDEBUG("** You are destroying a mutex which was never released. **\n");
       assert(0); // Destroyed mutex while in use
 
     }
 
-    if(_waiters.size() > 0) { 
+    if(_waiters.size() > 0) {
 
       ZTDEBUG("** You are destroying a mutex which is blocking %d threads. **\n", _waiters.size());
       assert(0); // Destroyed mutex while in use
@@ -125,7 +125,7 @@ MutexImpl<List, Behavior>::~MutexImpl()
 
   /**
    * Acquire a lock on the mutex. If this operation succeeds the calling
-   * thread holds an exclusive lock on this mutex, otherwise it is blocked 
+   * thread holds an exclusive lock on this mutex, otherwise it is blocked
    * until the lock can be acquired.
    *
    * @exception Deadlock_Exception thrown when the caller attempts to acquire() more
@@ -133,7 +133,7 @@ MutexImpl<List, Behavior>::~MutexImpl()
    * @exception Interrupted_Exception thrown when the caller status is interrupted
    * @exception Synchronization_Exception thrown if there is some other error.
    */
-template<typename List, typename Behavior> 
+template<typename List, typename Behavior>
 void MutexImpl<List, Behavior>::acquire() {
 
     ThreadImpl* self = ThreadImpl::current();
@@ -142,41 +142,41 @@ void MutexImpl<List, Behavior>::acquire(
     Monitor::STATE state;
 
     Guard<FastLock> g1(_lock);
-   
-    // Deadlock will occur if the current thread is the owner 
+
+    // Deadlock will occur if the current thread is the owner
     // and there is no entry count.
-    if(_owner == self) 
+    if(_owner == self)
       throw Deadlock_Exception();
-    
+
     // Acquire the lock if it is free and there are no waiting threads
     if(_owner == 0 && _waiters.empty()) {
 
       _owner = self;
 
-      ownerAcquired(self);
-      
+      MutexImpl<List, Behavior>::ownerAcquired(self);
+
     }
 
     // Otherwise, wait for a signal from a thread releasing its
     // ownership of the lock
-    else { 
-        
+    else {
+
       _waiters.insert(self);
       m.acquire();
 
-      waiterArrived(self);
+      MutexImpl<List, Behavior>::waiterArrived(self);
+
+      {
 
-      {        
-      
         Guard<FastLock, UnlockedScope> g2(g1);
         state = m.wait();
-      
+
       }
 
-      waiterDeparted(self);
+      MutexImpl<List, Behavior>::waiterDeparted(self);
 
       m.release();
-        
+
       // Remove from waiter list, regardless of wether release() is called or
       // not. The monitor is sticky, so its possible a state 'stuck' from a
       // previous operation and will leave the wait() w/o release() having
@@ -185,26 +185,26 @@ void MutexImpl<List, Behavior>::acquire(
       if(i != _waiters.end())
         _waiters.erase(i);
 
-      // If awoke due to a notify(), take ownership. 
+      // If awoke due to a notify(), take ownership.
       switch(state) {
         case Monitor::SIGNALED:
 
           assert(_owner == 0);
-          _owner = self;    
+          _owner = self;
 
-          ownerAcquired(self);
+          MutexImpl<List, Behavior>::ownerAcquired(self);
 
           break;
-        
+
         case Monitor::INTERRUPTED:
           throw Interrupted_Exception();
-        
+
         default:
           throw Synchronization_Exception();
-      } 
-    
+      }
+
     }
-  
+
   }
 
 
@@ -218,17 +218,17 @@ void MutexImpl<List, Behavior>::acquire(
    * @exception Interrupted_Exception thrown when the caller status is interrupted
    * @exception Synchronization_Exception thrown if there is some other error.
    */
-template<typename List, typename Behavior> 
+template<typename List, typename Behavior>
 bool MutexImpl<List, Behavior>::tryAcquire(unsigned long timeout) {
-  
+
     ThreadImpl* self = ThreadImpl::current();
     Monitor& m = self->getMonitor();
 
     Guard<FastLock> g1(_lock);
-   
-    // Deadlock will occur if the current thread is the owner 
+
+    // Deadlock will occur if the current thread is the owner
     // and there is no entry count.
-    if(_owner == self) 
+    if(_owner == self)
       throw Deadlock_Exception();
 
     // Acquire the lock if it is free and there are no waiting threads
@@ -236,39 +236,39 @@ bool MutexImpl<List, Behavior>::tryAcqui
 
       _owner = self;
 
-      ownerAcquired(self);
-      
+      MutexImpl<List, Behavior>::ownerAcquired(self);
+
     }
 
     // Otherwise, wait for a signal from a thread releasing its
     // ownership of the lock
     else {
-        
+
       _waiters.insert(self);
-    
+
       Monitor::STATE state = Monitor::TIMEDOUT;
-    
+
       // Don't bother waiting if the timeout is 0
       if(timeout) {
-      
+
         m.acquire();
 
-        waiterArrived(self);
-      
+        MutexImpl<List, Behavior>::waiterArrived(self);
+
         {
-        
+
           Guard<FastLock, UnlockedScope> g2(g1);
           state = m.wait(timeout);
-        
+
         }
 
-        waiterDeparted(self);
-      
+        MutexImpl<List, Behavior>::waiterDeparted(self);
+
         m.release();
-        
+
       }
-    
-    
+
+
       // Remove from waiter list, regarless of weather release() is called or
       // not. The monitor is sticky, so its possible a state 'stuck' from a
       // previous operation and will leave the wait() w/o release() having
@@ -276,96 +276,96 @@ bool MutexImpl<List, Behavior>::tryAcqui
       typename List::iterator i = std::find(_waiters.begin(), _waiters.end(), self);
       if(i != _waiters.end())
         _waiters.erase(i);
-    
-      // If awoke due to a notify(), take ownership. 
+
+      // If awoke due to a notify(), take ownership.
       switch(state) {
         case Monitor::SIGNALED:
-        
+
           assert(0 == _owner);
           _owner = self;
 
-          ownerAcquired(self);
-        
+          MutexImpl<List, Behavior>::ownerAcquired(self);
+
           break;
-        
+
         case Monitor::INTERRUPTED:
           throw Interrupted_Exception();
-        
+
         case Monitor::TIMEDOUT:
           return false;
-        
+
         default:
           throw Synchronization_Exception();
-      } 
-    
+      }
+
     }
-  
+
     return true;
-  
+
   }
 
   /**
    * Release a lock on the mutex. If this operation succeeds the calling
-   * thread no longer holds an exclusive lock on this mutex. If there are 
-   * waiting threads, one will be selected, assigned ownership and specifically 
+   * thread no longer holds an exclusive lock on this mutex. If there are
+   * waiting threads, one will be selected, assigned ownership and specifically
    * awakened.
    *
-   * @exception InvalidOp_Exception - thrown if an attempt is made to 
+   * @exception InvalidOp_Exception - thrown if an attempt is made to
    * release a mutex not owned by the calling thread.
    */
-template<typename List, typename Behavior> 
+template<typename List, typename Behavior>
 void MutexImpl<List, Behavior>::release() {
 
     ThreadImpl* impl = ThreadImpl::current();
 
     Guard<FastLock> g1(_lock);
-  
+
     // Make sure the operation is valid
     if(_owner != impl)
       throw InvalidOp_Exception();
 
     _owner = 0;
 
-    ownerReleased(impl);
-  
+    MutexImpl<List, Behavior>::ownerReleased(impl);
+
     // Try to find a waiter with a backoff & retry scheme
     for(;;) {
-    
+
       // Go through the list, attempt to notify() a waiter.
       for(typename List::iterator i = _waiters.begin(); i != _waiters.end();) {
-      
+
         // Try the monitor lock, if it cant be locked skip to the next waiter
         impl = *i;
         Monitor& m = impl->getMonitor();
 
         if(m.tryAcquire()) {
-  
-          // If notify() is not sucessful, it is because the wait() has already 
+
+          // If notify() is not sucessful, it is because the wait() has already
           // been ended (killed/interrupted/notify'd)
           bool woke = m.notify();
-          
+
           m.release();
-        
+
           // Once notify() succeeds, return
           if(woke)
             return;
-        
+
         } else ++i;
-      
+
       }
-    
+
       if(_waiters.empty())
         return;
 
       { // Backoff and try again
-      
+
         Guard<FastLock, UnlockedScope> g2(g1);
         ThreadImpl::yield();
 
       }
 
     }
-  
+
   }
 
 } // namespace ZThread
